# Projet BI

## Liens utiles:

- (Sujet du projet)[http://dac.lip6.fr/master/wp-content/uploads/2018/02/TP1-bi2018.pdf]
- (Lucidchart)[https://www.lucidchart.com/]

## TODO:

- Convertir json -> csv using csvkit

- Faire dimension Date(minute, heure, jour, mois, annee)
- Faire dimension Localisation (ville, pays, region, continent)
- Faire dimension Langue()
- Faire dimension Topic(tag)
- Faire dimension Utilisateur(compte officiel, nb followers, nb friends)
- Faire le fait Tweet (compte officiel, nb retweet, longeur texte, nb favourites)
- Faire le fait Utilisateur (compte officiel, nb followers, nb friends)
- scroller données Twitter

## Setup du projet

Charger la base de donnée world.

``` bash
mysql -s -u root
```

``` sql
create database world;
use world;
```

``` bash
mysql -s -u root < world.sql
```

Et voilaaa !
