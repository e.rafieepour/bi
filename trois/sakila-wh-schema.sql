CREATE TABLE staff (
  staff_id SMALLINT,
  full_name VARCHAR(50) NOT NULL
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE film (
  film_id SMALLINT,
  note INTEGER,
  category VARCHAR(50)
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE date (
  date_id SMALLINT,
  full_date TIMESTAMP,
  jour INTEGER,
  mois INTEGER,
  annee INTEGER,
  time VARCHAR(50)
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE customer (
  customer_id SMALLINT
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE shop (
  shop_id SMALLINT
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE rental (
  nb_locations INTEGER,
  nb_retours INTEGER,
  duree FLOAT,
  shop_id SMALLINT,
  date_id SMALLINT,
  film_id SMALLINT,
  staff_id SMALLINT,
  customer_id SMALLINT,
  FOREIGN KEY (shop_id) REFERENCES shop (shop_id),
  FOREIGN KEY (date_id) REFERENCES date (date_id),
  FOREIGN KEY (film_id) REFERENCES film (film_id),
  FOREIGN KEY (staff_id) REFERENCES staff (staff_id),
  FOREIGN KEY (customer_id) REFERENCES customer (customer_id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8;
